package tinder.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ResourcesOps {

    public static String dirUnsafe(String dir)  {
        try {
            return ResourcesOps.class
                    .getClassLoader()
                    .getResource(dir)
                    .toURI()
                    .getPath();

        } catch (URISyntaxException e) {
            throw new RuntimeException(String.format("Requested path `%s`not found", dir), e);
        }
    }

}
