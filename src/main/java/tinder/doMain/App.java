package tinder.doMain;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import tinder.db.Database;
import tinder.servlets.StaticContentServlet;
import tinder.servlets.UsersServlet;
import tinder.utils.ResourcesOps;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import java.sql.Connection;
import java.util.EnumSet;
import java.util.Optional;

public class App {

    static Optional<Integer> toInt(String raw) {
        try {
            return Optional.of(Integer.parseInt(raw));
        } catch (Exception x) {
            return Optional.empty();
        }
    }

    public static void run() throws Exception {
        Integer port = Optional.ofNullable(System.getenv("PORT"))
                .flatMap(App::toInt)
                .orElse(8080);

        Server server = new Server(port);

//        Database.checkAndApplyDeltas();
//        Connection conn = Database.conn();


        ServletContextHandler handler = new ServletContextHandler();

        String osStaticLocation = ResourcesOps.dirUnsafe("static");
        handler.addServlet(new ServletHolder(new StaticContentServlet(osStaticLocation)), "/static/*");
        handler.addServlet(new ServletHolder(new UsersServlet()), "/users");


        server.setHandler(handler);

        server.start();
        server.join();
    }
}
