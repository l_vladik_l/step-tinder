package tinder.interfaces;

public interface Identifable {
    Integer id();
}
