package tinder;

import lombok.SneakyThrows;
import tinder.doMain.App;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        App.run();
    }
}
